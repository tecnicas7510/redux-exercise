import { changeSample } from './changeSample';
import { updateOtherSample } from './updateOtherSample';

export { changeSample, updateOtherSample };
